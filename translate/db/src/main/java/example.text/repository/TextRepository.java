package example.text.repository;


import example.text.entity.Text;
import example.text.entity.User;
import example.text.entity.Word;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface TextRepository extends JpaRepository<Text, Long> {

    Text findById(Long id);


    @Transactional
    @Modifying
    @Query("update Text t set t.countPage = ?1 where t.id = ?2")
    void setCountPage(Integer count, Long id);

}
