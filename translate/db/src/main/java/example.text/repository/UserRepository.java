package example.text.repository;


import example.text.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByLogin(String login);

    User findByLoginAndPassword(String login, String password);

    User findById(Long id);


}
