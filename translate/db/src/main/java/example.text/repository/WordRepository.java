package example.text.repository;

import example.text.entity.Word;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;


public interface WordRepository extends JpaRepository<Word, Long> {

    Word findByCurrentWord(String currentWord);

    @Transactional
    void removeByCurrentWord(String currentWord);

}
