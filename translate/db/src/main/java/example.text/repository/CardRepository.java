package example.text.repository;


import example.text.entity.Card;
import example.text.entity.User;
import example.text.entity.Word;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CardRepository extends JpaRepository<Card, Long> {

    List<Card> findByUser(User user);

    @Transactional
    void removeByWordAndUser(Word word, User user);

    @Transactional
    @Modifying
    @Query("update Card c set c.isLearned = true where c.user = ?1 and c.word = ?2")
    void setLearnderStatus(User user, Word word);

    @Transactional
    @Modifying
    @Query("update Card c set c.isLearned = false where c.user = ?1 and c.word = ?2")
    void setUnLearnderStatus(User user, Word word);

    @Query("select c from Card c where c.isLearned = true and c.user = ?1")
    List<Card> getLearnedWord(User user);

    @Query("select c from Card c where c.isLearned = false and c.user = ?1")
    List<Card> getUnlearnedWord(User user);

    @Query("select c from Card c where c.user = ?1 and c.word = ?2")
    Card getStatus(User user, Word word);





}