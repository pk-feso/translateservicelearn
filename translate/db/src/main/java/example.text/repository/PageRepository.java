package example.text.repository;

import example.text.entity.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PageRepository extends JpaRepository<Page, Long> {


    Page findByPageAndTextId(int page, Long textId);

    @Query("select count(p) from Page p where p.textId = ?1")
    int getCountPage(Long textId);

}
