package example.text.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Table(name = "Texts")
@NoArgsConstructor
@ToString
@Entity
public class Text {
    @Id
    @GeneratedValue
    private Long id;
    private String title;
    @Column(length = Integer.MAX_VALUE)
    private String text;
    private int countPage;

    public Text(String title, String text) {
        this.title = title;
        this.text = text;
    }


}