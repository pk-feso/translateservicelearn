package example.text.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@ToString
@NoArgsConstructor
public class Card {
    @Id
    @GeneratedValue
    private Long id;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "word_id")
    private Word word;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;
    @Column(name = "learn")
    private boolean isLearned = false;

    public void learn() {
        isLearned = true;
    }

    public void forget() {
        isLearned = false;
    }

    public Card(Word word, User user, boolean isLearned) {
        this.word = word;
        this.user = user;
        this.isLearned = isLearned;
    }
}
