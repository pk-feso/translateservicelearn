package example.text.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "Words")
@Getter
@NoArgsConstructor
@ToString
public class Word {
    @Id
    @GeneratedValue
    @Column(name = "word_id")
    private Long id;
    @Column(name = "current_word")
    private String currentWord;
    private String translate;


    public Word(String currentWord, String translate) {
        this.currentWord = currentWord;
        this.translate = translate;
    }
}
