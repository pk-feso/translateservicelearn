package example.text.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Table(name = "Page")
@NoArgsConstructor
@ToString
@Entity
public class Page {
    @Id
    @GeneratedValue
    private Long id;
    private int page;
    @Column(length = Integer.MAX_VALUE)
    private String textOnPage;
    private Long textId;

    public Page(int page, String text, Long textId) {
        this.page = page;
        this.textOnPage = text;
        this.textId = textId;
    }
}
