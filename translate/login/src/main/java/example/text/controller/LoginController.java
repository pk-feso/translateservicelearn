package example.text.controller;

import example.text.entity.User;
import example.text.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

@Controller

public class LoginController {

    @Autowired
    LoginService service;

    @RequestMapping(value = "/register" , method = RequestMethod.POST)
    public String saveNewUser(String login, String password, HttpSession session) {
        service.saveUser(new User(login, password));
        User byLogin = service.findByLogin(login);
        session.setAttribute("user", byLogin);
        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginNewUser(String login, String password, HttpSession session) {
        User user = service.findByLoginAndPassword(login, password);
        session.setAttribute("user", user);
        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String getLoginPage() {
        return "login";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String getRegPAge() {
        return "register";
    }



}
