package example.text.service;

import example.text.entity.User;

public interface LoginService {

    void saveUser(User user);

    User findById(Long id);

    User findByLogin(String login);

    User findByLoginAndPassword(String login, String password);


}
