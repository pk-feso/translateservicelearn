package example.text.service;

import example.text.entity.User;
import example.text.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    UserRepository repository;


    @Override
    public void saveUser(User user) {
        User byLogin = repository.findByLogin(user.getLogin());
        if(byLogin != null) {
            throw new RuntimeException("Register yet");
        }
        repository.save(user);
    }

    @Override
    public User findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public User findByLogin(String login) {
        return repository.findByLogin(login);
    }

    @Override
    public User findByLoginAndPassword(String login, String password) {
        User byLoginAndPassword = repository.findByLoginAndPassword(login, password);
        if(byLoginAndPassword == null) {
            throw new RuntimeException("Not found");
        }
        return byLoginAndPassword;
    }


}
