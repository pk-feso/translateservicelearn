package example.text.http;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class OkHttpSender implements HttpSender {

    @Value("${api.translate}")
    private String API;
    @Value("${yandex.url}")
    private String url;

    @Autowired
    JsonParse parse;


    public String sendRequestOnTranslate(String translateWord) {
        Concat concat = (k,v) -> k + "=" + v;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url + "?" +
                        concat.addParam("key", API) + "&" +
                        concat.addParam("text", translateWord) + "&" +
                        concat.addParam("lang" ,"ru"))
                .build();
        String word;
        try {
            Response response = client.newCall(request).execute();
             word = (String) ((List) parse.parseJson(response.body().string()).get("text")).get(0);
        } catch (IOException e) {
            throw new RuntimeException("Error during translate");
        }
        return word;
    }

    private interface Concat {
        String addParam(String key, String param);
    }

}
