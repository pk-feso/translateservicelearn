package example.text.http;

import org.springframework.boot.json.GsonJsonParser;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class JsonParse {

    public Map<String, Object> parseJson(String json) {
        return new GsonJsonParser().parseMap(json);
    }

}
