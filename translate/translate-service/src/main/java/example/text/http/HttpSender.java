package example.text.http;

import com.squareup.okhttp.Response;

public interface HttpSender {

    String sendRequestOnTranslate(String translateWord);

}
