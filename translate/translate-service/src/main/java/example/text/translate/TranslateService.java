package example.text.translate;


import example.text.entity.Word;

public interface TranslateService {

    Word translate(String word);

}
