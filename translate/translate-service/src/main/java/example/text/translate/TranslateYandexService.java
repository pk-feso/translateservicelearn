package example.text.translate;

import example.text.entity.Word;
import example.text.http.HttpSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TranslateYandexService implements TranslateService {

    @Autowired
    HttpSender sender;


    @Override
    public Word translate(String word) {
        String translate;
        translate = sender.sendRequestOnTranslate(word);
        return new Word(word, translate);
    }



}
