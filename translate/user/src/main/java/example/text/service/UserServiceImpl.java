package example.text.service;

import example.text.dto.UserDto;
import example.text.entity.Card;
import example.text.entity.User;
import example.text.repository.CardRepository;
import example.text.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    CardRepository cardRepository;


    @Override
    public UserDto getUserInfo(User user) {
        User byId = userRepository.findById(user.getId());
        List<Card> learnedWord = cardRepository.findByUser(user);
        long unlearned = learnedWord.stream().filter(c -> !c.isLearned()).count();
        long learned = learnedWord.size() - unlearned;
        return new UserDto(byId, learned, unlearned);
    }
}
