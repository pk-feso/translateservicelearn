package example.text.service;

import example.text.dto.UserDto;
import example.text.entity.User;

public interface UserService {

    UserDto getUserInfo(User user);



}
