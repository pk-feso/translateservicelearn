package example.text.dto;

import example.text.entity.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@NoArgsConstructor
@ToString
public class UserDto {

    private String login;
    private long countLearnedWord;
    private long countAllWords;

    public UserDto(User byId, long countLearnedWord, long countAllWords) {
        this.login = byId.getLogin();
        this.countAllWords = countAllWords;
        this.countLearnedWord = countLearnedWord;
    }
}
