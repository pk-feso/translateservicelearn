package example.text.controller;


import example.text.dto.UserDto;
import example.text.entity.User;
import example.text.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
//// TODO: 15.11.16 extraccet new module user and dao 
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;


    @RequestMapping(params = "action=info", method = RequestMethod.GET)
    @ResponseBody
    public UserDto getUserInfo(HttpSession session) {
        return userService.getUserInfo((User) session.getAttribute("user"));
    }

}
