var Texts = [];


var Title = React.createClass({

    renderText: function () {
        ReactDOM.render(
            <Nothing/>,
            document.getElementById('container')
        );
        pagination(this.props.id)
    },

    render: function () {
        return (
            <li id={this.props.id} className="title" onClick={this.renderText}>
                <a>{this.props.title}</a>
            </li>
        );
    }

})

var Nothing = React.createClass({

    getInitialState: function () {
        $('#text').html("");
        $('#pagination').html("");
        return null;
    },


    render: function () {
        return (<div></div>)
    }
})

var User = React.createClass({

    getInitialState: function () {
        $('#text').html("");
        $('#pagination').html("");
        var CurrenUser = {
            login: null,
            countAllWords: 0,
            countLearnedWord: 0,
            renderType: "nothing"
        }
        return {CurrentUser: CurrenUser}
    },

    componentDidMount: function () {
        $.get('/user?action=info').done(function (data) {
            this.setState({CurrentUser: data});
        }.bind(this));
    },

    handelLearned: function () {
        this.refs.list.handleUpdateLearned();
    },

    handelUnlearned: function () {
        this.refs.list.handleUpdateUnLearned();

    },

    render: function () {
        var currentUser = this.state.CurrentUser;
        return <div>
            <b>Привет, {currentUser.login}</b>
            <p></p>
            <ul className="nav nav-tabs">
                <li onClick={this.handelLearned}><a>Словрь изученных слов</a></li>
                <li onClick={this.handelUnlearned}><a>Словарь изучаемых слов</a></li>
            </ul>
            <ListWords ref="list" typeRender="nothing"/>

        </div>
    }
})

var ListWords = React.createClass({

    getInitialState: function () {
        return {
            typeRender: 'nothing',
            listWords: [],
            listCurrentWords: []
        }
    },

    handleUpdateLearned: function () {
        $.get('/word?action=learned').done(function (data) {
            this.setState({
                listWords: data,
                listCurrentWords: data,
                typeRender: 'learned'
            });
        }.bind(this));
    },

    handleUpdateUnLearned: function () {
        $.get('/word?action=unlearned').done(function (data) {
            this.setState({
                listWords: data,
                typeRender: 'unlearned'
            });
        }.bind(this));
    },

    remove: function (word) {
        var map = this.state.listWords.map(function (current) {
            if (current.currentWord == word) {
                return null;
            } else {
                return current;
            }
        });
        map = map.filter(function (n) {
            return n != undefined
        });
        this.setState({
            listWords: map
        })

    },

    handleSearch: function (event) {
        var text = event.target.value;
        var words = this.state.listWords;
        var displayedContacts = words.filter(function (el) {
            var eng = el.currentWord.toLowerCase();
            var ru = el.translate.toLowerCase();
            return (eng.indexOf(text) !== -1) || (ru.indexOf(text) !== -1);
        });

        this.setState({
            listCurrentWords: displayedContacts
        })

    },


    render: function () {
        if (this.state.typeRender == 'nothing') {
            return (<div></div>);
        } else if (this.state.typeRender == 'unlearned') {
            return (<div>
                <ul id="list-texts" className="list-group">

                    {this.renderWord(this.state.listWords)}

                </ul>
                <b>
                    Колличество изучаемых слов {this.state.listWords.length}
                </b>
            </div>)
        } else if (this.state.typeRender == 'learned') {
            return (<div className="container">
                <p></p>
                <div className="row">
                    <div className="col-md-4">
                        <form action="" className="search-form">
                            <div className="form-group has-feedback">
                                <label form="search" className="sr-only">Search</label>
                                <input onChange={this.handleSearch} type="text" className="form-control" name="search"
                                       id="search" placeholder="search"/>
                                <div className="glyphicon glyphicon-search form-control-feedback"></div>
                            </div>
                        </form>
                    </div>
                </div>

                <ul id="list-texts" className="list-group">
                    {
                        this.state.listCurrentWords.map(function (el, index) {
                            return <Word key={index}
                                         word={el.currentWord}
                                         translate={el.translate}
                                         click={false}

                            />;
                        })
                    }

                </ul>
                <b>
                    Колличество изученных слов {this.state.listWords.length}
                </b>
            </div>)
        }
    },
    renderWord: function (words) {
        var out = [];
        for (var i = 0; i < words.length; i++) {
            out.push(<Word key={i} word={words[i].currentWord} translate={words[i].translate} remove={this.remove}
                           click={true}/>)
        }
        return out;
    }
})

var Word = React.createClass({

    getInitialState: function () {
        return {
            word: null,
            alertVisible: true
        }
    },

    handleSetStatusLearn: function () {
        $.post("/word?action=delete", {word: this.props.word}, function () {

        })
        this.props.remove(this.props.word)
    },

    render: function () {
        if (this.state.alertVisible) {
            if (this.props.click) {
                return (<li className="list-group-item">

                    <b>{this.props.word}</b> - {this.props.translate}
                    <div className="button2" style={{'float': 'right'}}
                         onClick={this.handleSetStatusLearn}>В словарь
                    </div>

                </li>)
            } else {
                return (<li className="list-group-item">
                    {this.props.word} - {this.props.translate}
                </li>)
            }
        } else {
            return null;
        }
    }

})


var UserLink = React.createClass({

    handlerUser: function () {
        ReactDOM.render(
            <User/>,
            document.getElementById('container')
        );

    },

    render: function () {
        return ( <li onClick={this.handlerUser}><a >User</a></li>)
    }
})


var TextLink = React.createClass({


    getInitialState: function () {

        return {
            displayedText: []
        };
    },

    componentDidMount: function () {
        $.get('/text?action=titles').done(function (data) {
            this.setState({displayedText: data});
        }.bind(this));
    },

    componentWillUpdate: function (next1, next) {
        console.log(this.state.displayedText)

    },

    componentWillReceiveProps: function (nextProps) {
        $.get('/text?action=titles').done(function (data) {
            console.log(data)
            this.setState({displayedText: data})
        }.bind(this))
    },

    handleUpdate: function () {
        $.get('/text?action=titles').done(function (data) {
            console.log(data)
            this.setState({displayedText: data})
        }.bind(this))
    },

    render: function () {

        return (<li className="dropdown" id="text_menu">
            <a onClick={this.handleUpdate} className="dropdown-toggle" data-toggle="dropdown">
                Text
                <b className="caret"></b>
            </a>
            <ul id="list-texts" className="dropdown-menu">
                {
                    this.state.displayedText.map(function (el) {
                        return <Title key={el.id}
                                      id={el.id}
                                      title={el.title}/>;
                    })
                }
            </ul>
        </li>)
    }
})


var AddLink = React.createClass({

    renderAdd: function () {
        ReactDOM.render(
            <Add/>,
            document.getElementById('container')
        );

    },

    render: function () {
        return ( <li onClick={this.renderAdd}><a>Add</a></li>)
    }
})


var Add = React.createClass({

    getInitialState: function () {
        $('#text').html("");
        $('#pagination').html("");
        return {
            title: null,
            text: null,
            render: 'nothing'
        };
    },


    setFileSend: function () {
        this.setState({
            render: 'file'
        })
    },
    setTextSend: function () {
        this.setState({
            render: 'text'
        })
    },

    render: function () {
        var text = "";
        return (<div>
                <p>Добавить текст</p>
                <ul className="nav nav-tabs">
                    <li onClick={this.setFileSend}><a>Загрзука файла</a></li>
                    <li onClick={this.setTextSend}><a>Загрзука текста</a></li>
                </ul>
                <div>
                    <TextSend render={this.state.render}/>
                    <FileSend render={this.state.render}/>
                </div>
            </div>
        )
    }

});

var TextSend = React.createClass({
    getInitialState: function () {
        return {isRender: this.props.render}
    },
    render: function () {
        if (this.props.render == 'text') {
            return <div style={{'margin-top': '20px'}}>
                <p>

                    <input value="Title" onChange={this.addTitle} type="text"/>

                </p>
                <p>
                    <textarea onChange={this.addText} style={{'width': '400px', 'height': '200px'}}/>
                </p>
                <button type="submit" onClick={this.handlerSend}>Отправить</button>
            </div>
        }
        return <div></div>
    },

    handlerSend: function () {
        $.post("/text?action=treatment", {title: this.state.title, text: this.state.text})
        ReactDOM.render(
            <HelloWorld />,
            document.getElementById('header')
        );


    },

    addTitle: function (event) {
        this.setState({
            title: event.target.value
        })
    },


    addText: function (event) {
        this.setState({
            text: event.target.value
        })
    }

})

var FileSend = React.createClass({


    render: function () {
        if (this.props.render == 'file') {
            return <div>
                asd </div>
        }
        return <div></div>
    }
})


var HelloWorld = React.createClass({


    render: function () {
        return (
            <div className="navbar navbar-default" id="header">
                <a className="navbar-brand">Translate</a>
                <ul style={{cursor: "pointer"}} className="nav navbar-nav" id="menu">
                    <UserLink rerender={this.rerender}/>
                    <TextLink/>
                    <AddLink/>
                </ul>
            </div>
        );
    }
});


ReactDOM.render(
    <HelloWorld />,
    document.getElementById('header')
);

