/**
 * Created by aleksejpluhin on 08.11.16.
 */



function pagination(textId) {
    var visible = 7;
    if(textId == undefined) return;
    $.get("/text?action=count_page&id=" + textId, function (count) {
        var page = count;
        if (page < visible) {
            visible = page;
        }
        extractedPag(page, visible);
    })


    function extractedPag(page, visible) {
        $("#pagination").twbsPagination('destroy');
        $('#pagination').twbsPagination({
            totalPages: page,
            visiblePages: visible,
            onPageClick: function (event, page) {
                getTextAndRenderByPage(textId, page);
            }
        });
    }

}

function getTextAndRenderByPage(id, idPage) {
    return $.get("/text?action=page&id=" + id + " &pageId=" + idPage, function (data) {
        $('#text').html(data);

    });
}

function destroy() {
    $('#pagination').html("");
}




