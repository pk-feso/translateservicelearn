/**
 * Created by aleksejpluhin on 06.11.16.
 */

$(document).on("click", "#text span", function () {
    if ($(this).hasClass("learn")) {
        $(this).removeClass("learn");
        deleteWord($(this).text());
    } else {
        $(this).addClass("learn");
        sendWord($(this).text());
    }
});

function renderWord(id) {
   $.get("/word?action=" + id, function (data) {
       var container = $('<ul/>');
       $.each(data, function (i, item) {
           addDiv(item);
           function addDiv() {
               var div;
               if(item.learned == true) {
                   div = $('<li/>').addClass("list-group-item list-group-item-success");
               } else {
                   div = $('<li/>').addClass("list-group-item");
               }
               var b1 = $('<b id=' + item.id + '/>').text(item.currentWord);
               var b2a = $('<b/>').text(" - " + item.translate);
               var b2 = $('<input class="learned-send" type="submit" style="float: right" value="Learned"/>');
               div.append(b1);
               div.append(b2a);
               div.append(b2);
               container.append(div);
           }
       });
       $('#list-words').html(container.html());
   })
}


$(document).on("click", "#word_menu .dropdown-menu li a", function () {
    renderWord($(this).attr("id"));
});

$(document).on("click", "#list-words .learned-send", function () {
    var word = $($(this).parent().find("b")[0]).text();
    $.post("/word?action=delete", {word : word}, function () {
    })
     $($(this).parent()).addClass("list-group-item-success")
});


function sendWord(text) {
    $.post("/word?action=save", {word : text},  function(data) {
    })

}
function deleteWord(text) {
    $.post("/word?action=delete", {word : text}, function (data) {

    })
}

