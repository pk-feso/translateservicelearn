package example.text.service;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;


@Service
public class Insertion {
    @Autowired
    TextService service;

    @Value("classpath:1.docx")
    private Resource resource;



    @PostConstruct
    public void text() throws IOException {
        InputStream inputStream =  resource.getInputStream();
        XWPFDocument wb = new XWPFDocument(inputStream);
        XWPFWordExtractor extractor = null;
        try
        {
            extractor = new XWPFWordExtractor(wb);
            service.treatmentText("Text1", extractor.getText());
        }
        catch (Exception exep)
        {
            exep.printStackTrace();
        }
    
    }


}
