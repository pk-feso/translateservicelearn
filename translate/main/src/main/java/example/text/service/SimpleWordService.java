package example.text.service;

import example.text.dto.WordDto;
import example.text.entity.Card;
import example.text.entity.User;
import example.text.entity.Word;
import example.text.repository.CardRepository;
import example.text.repository.WordRepository;
import example.text.translate.TranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SimpleWordService implements WordService {

    private final WordRepository repository;
    private final TranslateService service;
    private final CardRepository cardRepository;

    @Autowired
    public SimpleWordService(TranslateService service, WordRepository repository, CardRepository cardRepository) {
        this.service = service;
        this.repository = repository;
        this.cardRepository = cardRepository;
    }

    @Override
    public void putWord(String word) {
        Word translate = service.translate(word);
        Word byCurrentWord = repository.findByCurrentWord(word);
        if(byCurrentWord == null) {
            repository.save(translate);
        }
    }

    @Override
    public Word getTranslate(String word) {
        return repository.findByCurrentWord(word);
    }

    @Override
    public void setLearnedStatus(String word, User user) {
        Word byCurrentWord = repository.findByCurrentWord(word);
        cardRepository.setLearnderStatus(user, byCurrentWord);
    }

    @Override
    public List<WordDto> getWords(User user) {
        return cardRepository
                .findByUser(user).stream().map(c -> new WordDto(c.getWord(), c.isLearned()))
                .collect(Collectors.toList());
    }

    @Override
    public void saveWord(User user, String word) {
        Word currentWord = repository.findByCurrentWord(word);
        if(currentWord == null) {
            currentWord = service.translate(word);
            repository.save(currentWord);
        }
        Card card = cardRepository.getStatus(user, currentWord);
        if(card == null) {
            cardRepository.save(new Card(currentWord, user, false));
        } else if(card.isLearned()) {
            cardRepository.setUnLearnderStatus(card.getUser(), card.getWord());
        }
    }

    @Override
    public List<WordDto> getLearnedWords(User user) {
        return cardRepository.getLearnedWord(user).stream().map(c -> new WordDto(c.getWord(), true)).collect(Collectors.toList());
    }

    @Override
    public List<WordDto> getUnlearnedWords(User user) {
        return cardRepository.getUnlearnedWord(user).stream().map(c -> new WordDto(c.getWord(), false)).collect(Collectors.toList());
    }


}
