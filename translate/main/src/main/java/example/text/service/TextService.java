package example.text.service;

import example.text.dto.TextDto;
import example.text.dto.TitleDto;
import example.text.entity.User;

import java.util.List;

public interface TextService {
    void treatmentText(String title, String text);

    TextDto getText(Long id);

    TextDto getText(Long id, User user);

    List<TitleDto> getTitles();

    TextDto getTextByPage(Long textId, int page, User user);

    int getCountPage(Long id);
}

