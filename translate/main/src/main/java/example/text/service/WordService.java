package example.text.service;

import example.text.dto.WordDto;
import example.text.entity.User;
import example.text.entity.Word;

import java.util.List;

public interface WordService {

    void putWord(String word);

    Word getTranslate(String word);

    void setLearnedStatus(String word, User parse);


    List<WordDto> getWords(User user);

    void saveWord(User user, String word);


    List<WordDto> getLearnedWords(User user);

    List<WordDto> getUnlearnedWords(User user);

}
