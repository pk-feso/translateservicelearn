package example.text.service;

import example.text.entity.Page;
import example.text.repository.PageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class TreatmentPage {

    @Autowired
    private final PageRepository repository;

    public TreatmentPage(PageRepository repository) {
        this.repository = repository;
    }


    public int parse(String text, long textid) {
        AtomicInteger atomicInteger = new AtomicInteger(1);
        int counter = 0;
        char[] chars = text.toCharArray();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            if(" \n".contains("" + chars[i]) || i == chars.length - 1) {
                counter++;
                builder.append(chars[i]);
                if(counter == 1000 || i == chars.length - 1) {
                    buildWord(builder, textid, atomicInteger.getAndIncrement());
                    builder = new StringBuilder();
                    counter = 0;
                }
            } else {
                builder.append(chars[i]);
            }
        }
        return atomicInteger.get();
    }

    private void buildWord(StringBuilder builder, long textId, Integer page) {
        Page save = repository.save(new Page(page, builder.toString(), textId));
    }
}
