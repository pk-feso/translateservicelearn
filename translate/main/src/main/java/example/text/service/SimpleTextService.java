package example.text.service;


import example.text.dto.TextDto;
import example.text.dto.TitleDto;
import example.text.entity.Page;
import example.text.entity.Text;
import example.text.entity.User;
import example.text.render.Render;
import example.text.repository.PageRepository;
import example.text.repository.TextRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;



@Service
public class SimpleTextService implements TextService {

    private final TextRepository repository;
    private final Render render;
    @Autowired
    private final PageRepository pageRepository;
    @Autowired
    TreatmentPage treatmentPage;

    @Autowired
    public SimpleTextService(Render render, TextRepository repository, PageRepository pageRepository) {
        this.render = render;
        this.repository = repository;
        this.pageRepository = pageRepository;
    }

    @Override
    public void treatmentText(String title, String text) {
        Text save = repository.save(new Text(title, text));
        int parse = treatmentPage.parse(text, save.getId());
        repository.setCountPage(parse, save.getId());
    }

    @Override
    public TextDto getText(Long id) {
        return null;
    }

    @Override
    public TextDto getText(Long id, User user) {
        Text byId = repository.findById(id);
        String renderText = render.renderText(byId.getText(), user);
        return new TextDto(renderText);
    }

    @Override
    public List<TitleDto> getTitles() {
        List<Text> all = repository.findAll();
        return all.stream()
                .map(map -> new TitleDto(map.getId(), map.getTitle()))
                .collect(Collectors.toList());
    }

    @Override
    public TextDto getTextByPage(Long textId, int page, User user) {
        Page needPage =  pageRepository.findByPageAndTextId(page, textId);
        String renderText = render.renderText(needPage.getTextOnPage(), user);
        return new TextDto(renderText);
    }

    @Override
    public int getCountPage(Long id) {
        return pageRepository.getCountPage(id);
    }


}
