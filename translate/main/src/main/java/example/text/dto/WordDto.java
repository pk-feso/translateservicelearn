package example.text.dto;

import example.text.entity.Word;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class WordDto {
    private String currentWord;
    private String translate;
    private boolean isLearned;

    public WordDto(Word word, boolean isLearned) {
        this.currentWord = word.getCurrentWord();
        this.translate = word.getTranslate();
        this.isLearned = isLearned;
    }
}
