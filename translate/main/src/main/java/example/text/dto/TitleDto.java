package example.text.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class TitleDto {
    private Long id;
    private String title;

    public TitleDto(Long id, String title) {
        this.id = id;
        this.title = title;
    }

}
