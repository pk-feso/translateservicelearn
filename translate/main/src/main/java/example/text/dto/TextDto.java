package example.text.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class TextDto {

    private String renderText;

    public TextDto(String renderText) {
        this.renderText = renderText;
    }
}
