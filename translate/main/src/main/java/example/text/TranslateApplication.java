package example.text;

import example.text.entity.User;
import example.text.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;

@SpringBootApplication
@PropertySource(value = "test.properties")
@ComponentScan(basePackages = "example.text")
public class TranslateApplication {


    private final UserRepository userRepository;

    @Autowired
    public TranslateApplication(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @PostConstruct
    public void postCon() {
        userRepository.save(new User("test", "test"));
    }


    public static void main(String[] args) {
        SpringApplication.run(TranslateApplication.class, args);
    }
}
