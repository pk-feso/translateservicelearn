package example.text.controller;

import example.text.dto.TitleDto;
import example.text.entity.User;
import example.text.service.TextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/text")
public class TextController {

    private final TextService service;

    @Autowired
    public TextController(TextService service) {
        this.service = service;
    }

    @RequestMapping(params = "action=treatment", method = RequestMethod.POST)
    @ResponseBody
    public void treatment(String title, String text) {
        service.treatmentText(title, text);
    }

    @RequestMapping(params = "action=text", method = RequestMethod.GET)
    @ResponseBody
    public String getRenderText(@RequestParam(value = "id") Long id, HttpSession session) {
        return service.getText(id, (User) session.getAttribute("user")).getRenderText();
    }

    @RequestMapping(params = "action=titles", method = RequestMethod.GET)
    @ResponseBody
    public List<TitleDto> getTitles(HttpSession session) {
        return service.getTitles();
    }

    @RequestMapping(params = "action=page", method = RequestMethod.GET)
    @ResponseBody
    public String getRenderTextByPage(@RequestParam(value = "id") Long id,
                                      @RequestParam(value = "pageId") int pageId,
                                      HttpSession session) {
        return service.getTextByPage(id, pageId, (User) session.getAttribute("user")).getRenderText();
    }

    @RequestMapping(params = "action=count_page", method = RequestMethod.GET)
    @ResponseBody
    public int getRenderTextByPage(@RequestParam(value = "id") Long id) {
        return service.getCountPage(id);
    }


}
