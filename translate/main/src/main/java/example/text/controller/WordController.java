package example.text.controller;

import example.text.dto.WordDto;
import example.text.entity.User;
import example.text.entity.Word;
import example.text.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/word")
public class WordController {

    private final WordService service;

    @Autowired
    public WordController(WordService service) {
        this.service = service;
    }

    @RequestMapping(params = "action=translate", method = RequestMethod.GET)
    @ResponseBody
    public Word getTranslate(String word) {
        return service.getTranslate(word.toLowerCase());
    }

    @RequestMapping(params = "action=delete", method = RequestMethod.POST)
    @ResponseBody
    public void deleteWord(String word, HttpSession session) {
        service.setLearnedStatus(word.toLowerCase(), parse(session));
    }

    @RequestMapping(params = "action=all", method = RequestMethod.GET)
    @ResponseBody
    public List<WordDto> getWord(HttpSession session) {
        return service.getWords(parse(session));
    }

    @RequestMapping(params = "action=save", method = RequestMethod.POST)
    @ResponseBody
    public void save(String word, HttpSession session) {
        User user = parse(session);
        service.saveWord(user, word.toLowerCase());
    }

    @RequestMapping(params = "action=learned", method = RequestMethod.GET)
    @ResponseBody
    public List<WordDto> getLearnedWords(HttpSession session) {
        User user = parse(session);
        return service.getLearnedWords(user);
    }

    @RequestMapping(params = "action=unlearned", method = RequestMethod.GET)
    @ResponseBody
    public List<WordDto> getUnlearnedWords(HttpSession session) {
        User user = parse(session);
        return service.getUnlearnedWords(user);
    }

    private User parse(HttpSession session) {
        return  (User) session.getAttribute("user");
    }



}
