package example.text.render;

import example.text.entity.User;

public interface Render {

    public String renderText(String text, User user);


}
