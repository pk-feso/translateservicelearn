package example.text.render;

import example.text.entity.User;
import example.text.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SimpleRender implements Render {

    private final CardRepository repository;

    private static String regex = "[^\\p{L}\\p{Z}!{’\\-}]";

    @Autowired
    public SimpleRender(CardRepository repository) {
        this.repository = repository;
    }

    public String renderText(String text, User user) {
        Set<String> collect = getSet(user);
        char[] chars = text.toCharArray();
        StringBuilder b = new StringBuilder();
        StringBuilder buffer = new StringBuilder();
        String work;
        for (char ch : chars) {
            if (ch == '\n' || ch == ' ') {
                work = buffer.toString();
                buffer = new StringBuilder();
                String clear = work.replaceAll(regex, "");
                String span;
                if (collect.contains(clear.toLowerCase())) {
                    span = "<span class=\"learn\">" + clear + "</span>";
                } else {
                    span = "<span>" + clear + "</span>";
                }
                String replace = work.replace(clear, span);
                b.append(replace).append(ch);
                if (ch == '\n') {
                    b.append("     ");
                }
            } else {
                buffer.append(ch);
            }
        }
        return b.toString();
    }

    private Set<String> getSet(User user) {
        return repository
                .findByUser(user).stream().filter(c -> !c.isLearned()).map(c -> c.getWord().getCurrentWord())
                .collect(Collectors.toSet());
    }


}


